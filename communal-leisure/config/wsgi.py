import os
import sys

from django.core.wsgi import get_wsgi_application

sys.path.append('/var/www/communal-leisure/communal-leisure')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings.production")

application = get_wsgi_application()
