from django.conf.urls import include, url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from django.views.static import serve

import debug_toolbar
from rest_framework import routers

from apps import events
from apps.events.views import UpdateEvent, SubmitEventView
from apps.api.views import (EventViewSet,
                            EventCategoryViewSet,
                            ArticleViewSet,
                            MagazineViewSet)


router = routers.SimpleRouter()
router.register(r'events', EventViewSet, "events")
router.register(r'categories', EventCategoryViewSet, "categories")
router.register(r'articles', ArticleViewSet, "articles")
router.register(r'magazines', MagazineViewSet, "magazines")

urlpatterns = [
    url(r'^api/', include(router.urls)),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^', include('apps.events.urls')),
    url(r'^event/(?P<pk>[0-9]+)$', events.views.EventDetailView.as_view(), name='event_detail'),
    url('^event/(?P<pk>[\w-]+)/edit$', UpdateEvent.as_view(), name='update_event'),
    url(r'^magazine/', include('apps.articles.urls')),
    url(r'^submit-event/', SubmitEventView.as_view(), name='submit_event'),
    url(r'^get-fb-event/', events.views.get_fb_event, name='get_fb_event'),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

if settings.DEBUG:
    urlpatterns = [
                      url(r'^__debug__/', include(debug_toolbar.urls)),
                  ] + urlpatterns

    urlpatterns += [
        url(r'^media/(?P<path>.*)$', serve, {
            'document_root': settings.MEDIA_ROOT,
        }),
    ]

admin.site.site_header = 'Communal Leisure'
