from config.settings.base import *


DEBUG = True

def show_toolbar(request):
    return True

DEBUG_TOOLBAR_CONFIG = {
    'SHOW_TOOLBAR_CALLBACK': 'config.settings.development.show_toolbar',
}

SECRET_KEY= "$d(hw3c01*aj#l=3p1%m9(ae^%#*j%y5yq%(x9i9m9rx#-@c5s"

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'postgres',
        'USER': 'postgres',
        'HOST': 'db',
        'PORT': '5432',
    }
}

ALLOWED_HOSTS = [
    'comles_app_server',
    'unix:/var/www/communal-leisure/communal-leisure.sock',
    'communalleisure.com',
    '88.198.78.108',
    '127.0.0.1',
    '0.0.0.0'
]
