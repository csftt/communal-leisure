from base import *


SITE = "http://staging.communallesiure.com"

DEBUG = True

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'comles',
        'USER': 'comles',
        'PASSWORD': '0p3nm3',
        'HOST': 'localhost',
        'PORT': '',    
    }
}