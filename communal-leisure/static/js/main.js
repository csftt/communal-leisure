// using jQuery

var $grid = $('.grid');
var csrftoken = getCookie('csrftoken');

var fbToken;
var current_category = "";
function facebookLogin() {
    $.ajaxSetup({
        cache: true
    });
    $.getScript('//connect.facebook.net/en_US/sdk.js', function() {
        FB.init({
            appId: '424677944221708',
            version: 'v2.5'
        });

        FB.getLoginStatus(function(response) {
            if (response.status === 'connected') {
                fbToken = response.authResponse.accessToken;
            } else {
                FB.login();
            }
        });
    });
}
;
$("#id_image").change(function() {
    readURL(this);
});

$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
});

var filterFns = {
    free: function() {
        var number = $(this).data('cost');
        return parseInt(number) == 0;
    },

    category: function() {
        var category = $(this).data('category');
        return category == current_category;
    }
}

$('a.filter-button').unbind().on('click', function(e) {
    e.preventDefault();
    if ($(this).parent().hasClass('active')) {
        $grid.isotope({
            filter: '*'
        });
        $('.tablesorter').find('tr').show();
        $(this).parent().removeClass('active');
        history.pushState(null, '', '/');
        $('button.dropdown-toggle').removeClass('active');
    } else {
        var filterValue = $(this).attr('data-filter');
        if (filterValue == "category") {
            current_category = $(this).attr('data-category');
            $('.tablesorter').find('tr').show();
            $('.tablesorter').find('tr').not('.'.concat(current_category)).not('.tablesorter-headerRow').toggle();
            eventFilterQueryString([current_category]);
        } else if (filterValue == "free") {
            $('.tablesorter').find('tr').show();
            $('.tablesorter').find('tr').not('.free, .tablesorter-headerRow').toggle();
            eventFilterQueryString([filterValue]);
        }
        filterValue = filterFns[filterValue] || filterValue;

        $grid.isotope({
            filter: filterValue
        });
        $('.dropdown-menu').find('li').removeClass('active');

        $('button.dropdown-toggle').addClass('active');
        $(this).parent().addClass('active');
    }
    return true;
});

$('form').on('submit', function() {
    $.each($('#id_start_date, #id_end_date'), function() {
        $(this).attr('type', 'text').val($(this).val().replace("T", " "));
    });
});

$('.text-view-button').on('click', function() {
    $(this).toggleClass('active');
    $('.grid.row-fluid').fadeToggle(200).isotope();
    $('.tablesorter').fadeToggle(100);
});

$('.toggle-image-input').on('click', function() {
    $('.toggle-image-input').closest('li').toggle();
    $("input#id_download_image, input#id_image").val('');
});

$('.show-fb-import').on('click', function() {
    facebookLogin();
    $(this).fadeOut();
    $('.fbimport').show();
    $('.go').on('click', function() {
        populateEventFromFacebook();
        $('#id_download_image').parent().fadeIn();
        $('#id_image').parent().fadeOut();

    });

});

$('span.reveal').click(function() {
    $(this).parent().hide();
    $('.occur_wrapper').show();
});

$('#eventModal').on('hidden.bs.modal', function() {
    window.history.pushState({
        "html": "",
        "pageTitle": ""
    }, "", "/");
});

$('div.event').on('click', function() {
    var id = $(this).data('id');
    var toLoad = "/api/events/" + id + "/";
    var push = "/event/" + id;
    $.get(toLoad).then(function(data) {
        $("#eventModal-dialog").html(buildEvent(data));
        window.history.pushState({
            "html": "",
            "pageTitle": ""
        }, "", push);
        $('#eventModal').modal('show');
    });
});

$("img.lazy").lazyload({
    placeholder: "data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7",
    effect: "fadeIn",
    threshold: 500,
    failure_limit: Math.max($("img.lazy").length - 1, 0)
});

$(document).ready(function() {
    if (document.querySelectorAll('div.magazine-list-issues').length > 0) {
        htmlPaypalBuyNowButton();
    } else {
        var eventCats = [].slice.call(document.querySelectorAll('.grid-item.event')).map(function(x) {
            return x.dataset.category
        });
        [].slice.call(document.querySelectorAll('.filter-button')).forEach(function(x) {
            if (eventCats.indexOf(x.dataset.category) < 0) {
                x.parentNode.style.display = 'none'
            }
        });
    }
    $("form input").prop('required', true);
    $('input#id_download_image').prop('required', false);
    $('#id_start_date, #id_end_date').attr('type', 'datetime-local');
    $('#id_download_image').parent().hide();

    $("select#id_occur, label[for=id_occur]").wrapAll("<div class='occur_wrapper' />");
    $('.occur_wrapper').hide();
    $('.occur_wrapper').parent().prepend("<small>Click <span class='reveal'>here</span> if you need to add the same poster and details for a weekly or biweekly event</small>");

    $("input#id_image").closest('li').append("<div><small>click <span class='toggle-image-input'>here</span> to download an image from a url instead</small></div>");
    $("input#id_download_image").closest('li').append("<div><small>click <span class='toggle-image-input'>here</span> to upload an image instead</small></div>");

    $('.grid-item-info').textfill();

    $('.tablesorter').tablesorter();

});

$(window).load(function() {
    $grid.imagesLoaded().progress(function() {
        initIsotope($grid);
    });

    hoverInfo();
    var qsFilter = getParameterByName("cats");
    if (qsFilter != null) {
        if (qsFilter.toLowerCase() == "free") {
            var filterButtonToClickOn = document.querySelectorAll("[data-filter='Free']")[0];
        } else {
            var filterButtonToClickOn = document.querySelectorAll("[data-category='" + qsFilter + "']")[0];
        }
        filterButtonToClickOn.click();
    }

});

$(document).ajaxStart(function() {
    $('#loading-rabbit').show();
}).ajaxStop(function() {
    $('#loading-rabbit').hide();
});
