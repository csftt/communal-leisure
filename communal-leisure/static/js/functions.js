function buildEvent(data) {
var body = '<div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">�</span><span class="sr-only">Close</span></button><h4 class="modal-title" id="addEvent">'
+data.description.split("\n")[0]+
'</h4></div><div class="modal-body"><ul><li>' 
+new Date(data.start_date).toUTCString()+'</li>'
+'<li>'+data.acts+'</li>'
+'<li>�'+data.cost+'</li>'
+'<li>'+data.venue+'</li>'
+'</ul><p>'+data.description
+'</p><hr><img src="'+data.image+
'" alt="event"></div><div class="modal-footer"><button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button></div></div>';
return body;
}

$.fn.fitText = function() {
var _this = this;
var desired_height = _this.parent().find('img').height();
var size = Array.from(Array(50).keys()).filter(function(x){
_this.css("font-size", x);
var current_height = _this.get(0).scrollHeight;
if(current_height <= desired_height){
return x;
}
}).slice(-1).pop();
_this.css("font-size", size);
};

function hoverInfo() {
$('.grid-item.event').bind("mouseenter", function()
{
  $(this).find('.grid-item-info').stop(true).show();
if(!$(this).hasClass('fitText')){
$(this).find('.grid-item-info').fitText();
$(this).addClass('fitText');
}

});

$('.grid-item.event').bind("mouseleave", function()
{
  $(this).find('.grid-item-info').stop(true).hide();
});

}

function getParameterByName(name, url) {
    if (!url) {
      url = window.location.href;
    }
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function loadFonts(){
var fontFile = navigator.userAgent.match(/msie/i) ? '/static/css/fonts/Benguiat.eot' 
    : '/static/css/fonts/Benguiat.otf';
 $.ajax({
    url: fontFile,
    beforeSend: function ( xhr ) {
      xhr.overrideMimeType("application/octet-stream");
    },
    success: function(data) {
      $("<link />", {
        'rel': 'stylesheet',
        'href': '/static/css/fonts.css'
      }).appendTo('head');
    }
  });
}

function fbDetail(access_token, fields){
var event_id = $('#eventid').val().split('/events/')[1].replace('/','');
if(event_id.split('?').length>1){
event_id = event_id.split('?')[0];
}
return $.getJSON('/get-fb-event/',
{
 event_id:event_id,
 fields:fields,
 access_token: access_token
});
}

function fillBasic(data){
$('input#id_start_date').val(data.start_time.split(':00+')[0]);
$('input#id_end_date').val(data.end_time!=undefined ? data.end_time.split(':00+')[0]:data.start_time.split(':00+')[0]);
$('textarea#id_description').val(data.name+'\n\n'+data.description+'\n\n https://www.facebook.com/events/'+data.id);
if(data.place!=undefined){
$('#id_venue').val(data.place.name+', '+(data.place.street!=undefined?data.place.street:""));
}
}


function fillPoster(data){
$('input#id_image').prop('required',false);
if(data.cover!=undefined){
$('input#id_download_image').val(data.cover.source);
$('.modal-content').css('background-image', "url('"+data.cover.source+"')");
}
}

function populateEventFromFacebook(){
facebookLogin();
fbDetail(fbToken)
 .then(fillBasic, function(error){ console.log("error getting details from facebook", error) });


fbDetail(fbToken,'cover')
 .then(fillPoster, function(error){ console.log("error getting details from facebook", error) });
}


function readURL(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('.modal-content').css('background-image', "url('"+e.target.result+"')");
        }

        reader.readAsDataURL(input.files[0]);
    }
}

function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

 function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}

function initIsotope($grid){ 
 $grid.isotope({
  getSortData: {
    date: function( itemElem ) { 
      var date = $( itemElem ).data('date');
      return parseInt(date);
    },
  },
  sortBy : 'date',  
  layoutMode: 'masonry',
  itemSelector: '.grid-item',
  percentPosition: true,
  masonry: {
  columnWidth: 30
}
});
};

var eventFilterQueryString = function(categories){
var qs = categories.reduce(function(result,x,i){
return result+x+(i != categories.length-1 ? "|" : "")
},"/?cats=");
history.pushState(null,'/',qs);
};


function htmlPaypalBuyNowButton(){
	 var text = document.createTextNode('Get Issue 3 Delivered. �2 UK, �4 Europe, �6 Worldwide')
	 var poundSpan = document.createElement('span');
	 poundSpan.append(document.createTextNode('�'))
	 poundSpan.style.fontWeight="bold";
	 var container = document.createElement('div');
	 	 container.id = "paypal";
	container.style.clear='both';
	  var paypalFormInputs = [
	 {"type":"hidden","name":"business","value":"neon42@gmail.com"},
	 {"type":"hidden","name":"cmd","value":"_xclick"},
	 {"type":"hidden","name":"item_name","value":"communal leisure issue 3 delivery"},
	 {"type":"hidden","name":"currency_code","value":"GBP"},
	 {"type":"hidden","name":"return","value":"https://communalleisure.com/magazine"}
	 ];
	 var amountInput = document.createElement('input');
	 amountInput.type="number";
	 amountInput.min=2;
         amountInput.step=2;
	 amountInput.name="amount";
	 amountInput.value=2;

	 var form = document.createElement('form');
     form.method = "post";
     form.action = "https://www.paypal.com/cgi-bin/webscr";

	 paypalFormInputs.forEach(function(obj){
	 	var input = document.createElement('input');
     	input.name = obj.name;
     	input.type=obj.type;
     	input.value = obj.value;
     	form.append(input);
	 })
	 form.append(poundSpan);
	 form.append(amountInput);

	 var imgInput = document.createElement('input');
	 imgInput.type="image";
	 imgInput.name="submit";
	 imgInput.border=0;
	 imgInput.src="https://www.paypalobjects.com/webstatic/en_US/i/btn/png/btn_buynow_107x26.png";
	 imgInput.alt="Buy Now";
	 imgInput.style.display='block';

	 form.append(imgInput);

	 var image = document.createElement('img');
	 image.alt="";
	 image.border=0;
	 image.width=1;
	 image.height=1;
	 image.src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif";

	 form.append(image);

  	 container.append(text);
     container.append(form);
     
var parent = document.querySelectorAll('div.magazine-list-issues')[0];
var col = document.createElement('div');
col.classList.add('col');
col.append(container);
parent.append(col);
}
