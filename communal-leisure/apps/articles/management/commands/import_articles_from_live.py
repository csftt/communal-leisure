import json
import requests

from django.conf import settings
from django.core.management.base import BaseCommand
from django.core.files.temp import NamedTemporaryFile
from django.core.files import File
from django.core.urlresolvers import reverse

from apps.articles.models import Article, Magazine


class Command(BaseCommand):
    help = "Import articles and magazines from live"

    def handle(self, *args, **options):
        articles = json.loads(
            requests.get("{}{}".format(
                settings.SITE, reverse("articles-list"))
            ).content
        )

        for article in articles:
            if not Article.objects.filter(pk=article["pk"]).exists():
                new_article = Article.objects.create(
                    pk=article["pk"],
                    article_body=article["article_body"],
                    date_created=article["date_created"],
                    name=article["name"],
                    attachment_name=article["attachment_name"]
                )

                if article["image"] is not None:
                    image = NamedTemporaryFile(delete=True)
                    image.write(requests.get(
                        article["image"], stream=True).content
                    )
                    image.flush()

                    new_article.image.save(
                        article["image"].split("/")[-1],
                        File(image)
                    )

                if article["attachment"] is not None:
                    attachment = NamedTemporaryFile(delete=True)
                    attachment.write(requests.get(
                        article["attachment"], stream=True).content
                    )
                    attachment.flush()

                    new_article.attachment.save(
                        article["attachment"].split("/")[-1],
                        File(attachment)
                    )

        magazines = json.loads(
            requests.get("{}{}".format(
                settings.SITE, reverse("magazines-list"))
            ).content
        )

        for magazine in magazines:
            if not Magazine.objects.filter(pk=magazine["pk"]).exists():
                new_magazine = Magazine.objects.create(
                    pk=magazine["pk"],
                    date_created=magazine["date_created"]
                )

                magazine_image = NamedTemporaryFile(delete=True)
                magazine_image.write(requests.get(
                    magazine["image"], stream=True).content
                )
                magazine_image.flush()

                new_magazine.image.save(
                    magazine["image"].split("/")[-1],
                    File(magazine_image)
                )

                pdf = NamedTemporaryFile(delete=True)
                pdf.write(requests.get(
                    magazine["pdf"], stream=True).content
                )
                pdf.flush()

                new_magazine.pdf.save(
                    magazine["pdf"].split("/")[-1],
                    File(pdf)
                )
