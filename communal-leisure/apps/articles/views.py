from datetime import datetime

from django.views.generic.list import ListView
from django.views.generic.detail import DetailView

from apps.articles.models import Article, Magazine


class MagazineListView(ListView):
    model = Magazine
    ordering = '-date_created'

    def get_queryset(self, **kwargs):
        return Magazine.objects.filter(published=True)

    def get_context_data(self, **kwargs):
        context = super(MagazineListView, self).get_context_data(**kwargs)

        context['articles'] = Article.objects.all().order_by('-date_created')
        return context


class ArticleDetailView(DetailView):
    model = Article
