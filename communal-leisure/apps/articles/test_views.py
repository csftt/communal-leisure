from django.core.urlresolvers import reverse
from django.test import TestCase

from apps.articles.models import Magazine, Article


class MagazineTests(TestCase):
    def setUp(self):
        self.article = Article.objects.create()
        self.magazine = Magazine.objects.create()
        self.unpublished_magazine = Magazine.objects.create(published=False)

    def test_magazine_list(self):
        response = self.client.get(reverse("articles"))
        self.assertIn(self.magazine, response.context["object_list"])
        self.assertIn(self.article, response.context["articles"])
        self.assertNotIn(self.unpublished_magazine, response.context["object_list"])

    def test_article_detail(self):
        response = self.client.get(reverse("article-detail", kwargs={"pk": self.article.id}))
        self.assertEqual(response.context['object'], self.article)
