from django.test import TestCase

from apps.articles.models import Magazine, Article


class ArticlesTests(TestCase):
    def setUp(self):
        Article.objects.create()
        Magazine.objects.create()

    def test_article_creation(self):
        self.assertTrue(Article.objects.all().exists())

    def test_magazine_creation(self):
        self.assertTrue(Magazine.objects.all().exists())
 