from django.contrib import admin

from apps.articles.models import Article, Magazine


class ArticleAdmin(admin.ModelAdmin):
    exclude = ('created_by', )

    def get_queryset(self, request):
        qs = super(ArticleAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(created_by=request.user)

    def save_model(self, request, obj, form, change): 
        if obj.created_by is None:
            obj.created_by = request.user
        obj.save()


class MagazineAdmin(admin.ModelAdmin):
    pass


admin.site.register(Magazine, MagazineAdmin)
admin.site.register([Article], ArticleAdmin)
