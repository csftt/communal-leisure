from django.conf.urls import url

from apps.articles.views import MagazineListView, ArticleDetailView


urlpatterns = [
    url(r'^$', MagazineListView.as_view(), name='articles'),
    url(r'^(?P<pk>[0-9]+)/$', ArticleDetailView.as_view(), name='article-detail'),
]
