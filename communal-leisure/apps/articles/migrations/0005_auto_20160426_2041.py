# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-04-26 20:41
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('articles', '0004_auto_20160426_2014'),
    ]

    operations = [
        migrations.AlterField(
            model_name='article',
            name='created_by',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='created_by', to=settings.AUTH_USER_MODEL),
        ),
    ]
