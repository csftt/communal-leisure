# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('articles', '0008_article_attachment_name'),
    ]

    operations = [
        migrations.AlterField(
            model_name='article',
            name='attachment',
            field=models.FileField(null=True, blank=True, upload_to='attachments/%Y/%m/%d'),
        ),
    ]
