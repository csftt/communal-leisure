# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-06-17 12:17
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('articles', '0007_article_attachment'),
    ]

    operations = [
        migrations.AddField(
            model_name='article',
            name='attachment_name',
            field=models.CharField(blank=True, max_length=200, null=True),
        ),
    ]
