from datetime import date

from django.db import models
from django.conf import settings
from django.contrib.auth.models import User


class Article(models.Model):
    article_body = models.TextField()
    date_created = models.DateTimeField(auto_now=True)
    created_by = models.ForeignKey(User, null=True,
                                      related_name='created_by')
    image = models.ImageField(blank=True, null=True, upload_to='posters/%Y/%m/%d')
    name = models.CharField(max_length=200)
    attachment = models.FileField(blank=True, null=True, upload_to='attachments/%Y/%m/%d')
    attachment_name = models.CharField(max_length=200, blank=True, null=True)

    def __str__(self):
        return self.name


class Magazine(models.Model):
    date_created = models.DateTimeField(auto_now=True)
    published = models.BooleanField(default=True)
    image = models.ImageField(blank=True, null=True, upload_to='magazines/')
    pdf = models.FileField(blank=True, null=True, upload_to='magazines/pdfs/')
