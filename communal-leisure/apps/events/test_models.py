from datetime import datetime

from django.test import TestCase

from apps.events.models import Event, EventCategory


class EventsTests(TestCase):
    def setUp(self):
        Event.objects.create(start_date=datetime.now(), end_date=datetime.now())
        EventCategory.objects.create()

    def test_event_creation(self):
        self.assertTrue(Event.objects.all().exists())

    def test_event_category_creation(self):
        self.assertTrue(EventCategory.objects.all().exists())
 