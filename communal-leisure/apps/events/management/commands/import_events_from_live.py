from datetime import datetime
import json
import requests

from django.conf import settings
from django.core.management.base import BaseCommand
from django.core.files import File
from django.core.files.temp import NamedTemporaryFile
from django.core.urlresolvers import reverse

from apps.events.models import Event, EventCategory


class Command(BaseCommand):
    help = "Imports events from the live site"

    def handle(self, *args, **options):
        categories = json.loads(
            requests.get("{}{}".format(
                settings.SITE, reverse("categories-list"))
            ).content
        )

        for category in categories:
            if not EventCategory.objects.filter(name=category["name"]).exists():
                EventCategory.objects.create(
                    display_name=category["display_name"],
                    name=category["name"]
                )

        events = json.loads(requests.get(
            "{}{}?after_date={}".format(
                settings.SITE,
                reverse("events-list"),
                datetime.now().strftime("%Y-%m-%d")
            )
        ).content)

        for event in events:
            if not Event.objects.filter(pk=event["pk"]).exists():
                image = NamedTemporaryFile(delete=True)
                image.write(requests.get(event["image"], stream=True).content)
                image.flush()

                category_data = event.get("category", None)

                if category_data is not None:
                    category = EventCategory.objects.get(
                        name=category_data["name"]
                    )
                else:
                    category = None

                new_event = Event.objects.create(
                    pk=event["pk"],
                    description=event["description"],
                    acts=event["acts"],
                    venue=event["venue"],
                    start_date=event["start_date"],
                    end_date=event["end_date"],
                    cost=event["cost"],
                    occur=event["occur"],
                    published=True,
                    category=category
                )

                new_event.image.save(
                    event["image"].split("/")[-1],
                    File(image)
                )
