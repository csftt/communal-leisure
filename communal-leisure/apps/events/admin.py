from django.contrib import admin
from django.contrib.auth.models import Group, User

from apps.events.models import Event, EventCategory
from apps.events.forms import EventForm


class EventAdmin(admin.ModelAdmin):
    search_fields = ('acts', 'description', )

class EventCategoryAdmin(admin.ModelAdmin):
    pass

admin.site.register([EventCategory], EventCategoryAdmin)

admin.site.register([Event], EventAdmin)
