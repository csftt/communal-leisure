# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0006_auto_20160723_2015'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='occur',
            field=models.CharField(max_length=1, choices=[('0', 'Once'), ('1', 'Weekly'), ('2', 'BiWeekly'), ('3', 'Monthly')], default='0'),
        ),
    ]
