# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0002_auto_20160414_2306'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='download_image',
            field=models.URLField(blank=True),
        ),
        migrations.AlterField(
            model_name='event',
            name='image',
            field=models.ImageField(blank=True, upload_to='posters/%Y/%m/%d'),
        ),
    ]
