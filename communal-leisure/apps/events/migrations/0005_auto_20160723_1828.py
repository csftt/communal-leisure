# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0004_auto_20160424_0916'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='edit_key',
            field=models.CharField(max_length=50, blank=True),
        ),
        migrations.AddField(
            model_name='event',
            name='email',
            field=models.EmailField(max_length=254, blank=True),
        ),
    ]
