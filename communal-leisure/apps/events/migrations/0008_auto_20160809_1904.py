# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0007_event_occur'),
    ]

    operations = [
        migrations.AlterField(
            model_name='event',
            name='occur',
            field=models.CharField(max_length=1, default='0', choices=[('0', 'Once'), ('1', 'Weekly'), ('2', 'BiWeekly')]),
        ),
    ]
