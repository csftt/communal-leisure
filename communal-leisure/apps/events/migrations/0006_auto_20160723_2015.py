# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0005_auto_20160723_1828'),
    ]

    operations = [
        migrations.AlterField(
            model_name='event',
            name='edit_key',
            field=models.CharField(null=True, max_length=50, blank=True),
        ),
    ]
