from django.forms import ModelForm, ModelChoiceField

from apps.events.models import Event


class EventForm(ModelForm):
    class Meta:
        model = Event
        fields = ['image', 'download_image', 'acts', 'venue', 'cost', 'start_date', 'end_date', 'occur', 'description', 'email']
        exclude = ['edit_key']
