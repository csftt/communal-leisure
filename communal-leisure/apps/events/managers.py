from itertools import chain

from datetime import datetime, timedelta

from django.db import models


class EventManager(models.Manager):
    def forthcoming_events(self, **kwargs):
        return self.filter(
            published=True,
            end_date__gte=datetime.now(),
            occur=0
        ).order_by('start_date')

    def old_events(self, **kwargs):
        return self.filter(published=True).order_by('end_date')

    def weekly_events(self, **kwargs):
        events = self.filter(occur=1, published=True)
        for event in events:
            while event.end_date <= datetime.now():
                event.start_date = event.start_date + timedelta(days=7)
                event.end_date = event.end_date + timedelta(days=7)

        return events

    def biweekly_events(self, **kwargs):
        events = self.filter(occur=2, published=True)

        for event in events:
            while event.end_date <= datetime.now():
                event.start_date = event.start_date + timedelta(days=14)
                event.end_date = event.end_date + timedelta(days=14)

        return events

    def all_events(self, year=None, **kwargs):
        if year != "2016":
            all_events = list(chain(
                self.forthcoming_events(),
                self.weekly_events(),
                self.biweekly_events()
            ))
        else:
            all_events = list(chain(
                self.old_events(),
                self.weekly_events(),
                self.biweekly_events()
            ))

        return sorted(all_events, key=lambda x: x.start_date, reverse=False)