from django.conf.urls import url

from apps.events import views


urlpatterns = [
    url(r'^$', views.EventListView.as_view(), name='events'),
    url(r'^event/(?P<pk>[0-9]+)/$', views.EventDetailView.as_view(), name='detail'),
    url(r'^submit-event/', views.SubmitEventView.as_view(), name='submit_event'),
]
