import tempfile
import urllib
from PIL import Image

from django.db import models
from datetime import date, datetime
from django.conf import settings

from apps.events.managers import EventManager


class Event(models.Model):
    image = models.ImageField(null=True, blank=True, upload_to='posters/%Y/%m/%d')
    download_image = models.URLField(blank=True)
    venue = models.CharField(max_length=200)
    acts = models.CharField(max_length=200)
    start_date = models.DateTimeField(blank=False)
    end_date = models.DateTimeField(blank=False)
    cost = models.DecimalField(max_digits=6, decimal_places=2, null=True)
    description = models.TextField(null=True, blank=True)
    email = models.EmailField(max_length=254, blank=True)
    category = models.ForeignKey('EventCategory', null=True, blank=True)

    published = models.NullBooleanField(blank=True)  
    ONCE = '0'
    WEEKLY = '1'
    BIWEEKLY = '2'
    OCCUR_CHOICES = (
        (ONCE, 'Once'),
        (WEEKLY, 'Weekly'),
        (BIWEEKLY, 'BiWeekly'),
    )
    occur = models.CharField(max_length=1,
                             choices=OCCUR_CHOICES,
                             default=ONCE)
    edit_key = models.CharField(max_length=50, blank=True, null=True)

    objects = EventManager()
 
    def __str__(self):
        if self.description:
            return self.description.splitlines()[0]
        else:
            return str(self.pk)


class EventCategory(models.Model):
    display_name = models.CharField(max_length=200, blank=True)
    name = models.CharField(max_length=200, blank=True)

    def __str__(self, *args, **kwargs):
        return self.name

    class Meta:
        verbose_name_plural = "Event categories"
