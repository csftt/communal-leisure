
from facepy import GraphAPI
import requests
import tempfile
import urllib
import random
from itertools import chain
import string
from PIL import Image

from django.core.files.base import ContentFile
from django.core.files import File
from django.views.generic import UpdateView, DetailView, ListView, FormView
from django.views.decorators.csrf import ensure_csrf_cookie, csrf_exempt
from django.shortcuts import HttpResponse, redirect
from django.core.paginator import Paginator, InvalidPage, EmptyPage
from django.template import RequestContext, loader
from django.shortcuts import get_object_or_404, render
from django.core.mail import send_mail
from django.forms import modelform_factory
from django.conf import settings
from django.core.urlresolvers import reverse
from django.http import JsonResponse
from django.core import files

from apps.events.forms import EventForm
from apps.events.models import Event, EventCategory


class EventListView(ListView):
    model = Event
    template_name = "events/events.html"
    paginate_by = 100
    context_object_name = "events"
    ordering = "start_date"

    def get_queryset(self, **kwargs):
        return Event.objects.all_events(
            year=self.request.GET.get('year', '')
        )

    def get_context_data(self, **kwargs):
        context = super(EventListView, self).get_context_data(**kwargs)
        context["categories"] = EventCategory.objects.all()
        context["formset"] = modelform_factory(
            Event, exclude=('published','edit_key',)
        )
        context["site_url"] = settings.SITE
        return context


class EventDetailView(DetailView):
    model = Event
    template_name = "events/events.html"

    def get_context_data(self, **kwargs):
        context = super(EventDetailView, self).get_context_data(**kwargs)
        context['events'] = Event.objects.forthcoming_events
        context['formset'] = modelform_factory(
            Event, exclude=('published','edit_key',)
        )
        return context


class UpdateEvent(UpdateView):
    model = Event
    form_class = EventForm
    template_name = 'events/edit.html'
    success_url = '/'


class SubmitEventView(FormView):
    form_class = modelform_factory(Event, exclude=('published', ))
    success_url = '/' #reverse("events") + "?add=success"

    # @method_decorator(csrf_exempt)
    # def dispatch(self, request, *args, **kwargs):
    #     return super(SubmitEventView, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        new_event = form.save()

        if form.data['download_image']:
            imgurl = form.cleaned_data.get('download_image')
            filename = imgurl.split('/')[-1].split('.')[0]
            img_temp = tempfile.NamedTemporaryFile(delete=True)
            img_temp.write(urllib.request.urlopen(imgurl).read())
            img_temp.flush()
            img_save = Image.open(img_temp)
            img_save.save(
                settings.MEDIA_ROOT + '/posters/' + filename + '.jpg'
            )
            new_event.image = 'posters/' + filename + '.jpg'

        new_event.edit_key = ''.join(
            random.SystemRandom().choice(
                string.ascii_uppercase + string.digits
            ) for _ in range(6)
        )
        new_event.published = True
        new_event.save()

        email_text=[]
        email_text.append('Event: '+new_event.description.splitlines()[0])
        email_text.append('Email: '+new_event.email)
        email_text.append(
            'Edit Link: {0}/event/{1}/edit?key={2}'.format(
                settings.SITE, new_event.id,new_event.edit_key)
        )

        send_mail(
            'communal leisure event edit key',
            '\n\n'.join(email_text),
            'comlesweb@gmail.com',
            [new_event.email, 'comlesweb@gmail.com'],
            fail_silently=False
        )

        return super(SubmitEventView, self).form_valid(form)

    def form_invalid(self, form):
        return redirect('/') #reverse("events")+'?add=error')


def get_fb_event(request):
    event_id = request.GET.get('event_id', 'me')
    fields = request.GET.get('fields','')
    access_token = request.GET.get('access_token','')
    graph = GraphAPI(access_token)
    data = graph.get('/'+event_id+'?fields='+fields)

    return JsonResponse(data)

