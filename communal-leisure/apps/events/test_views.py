from datetime import datetime, timedelta

from django.test import TestCase
from django.core.urlresolvers import reverse
from django.template.context_processors import request

from apps.events.models import Event, EventCategory


class EventViewsTests(TestCase):
    def setUp(self):
        self.event = Event.objects.create(
            start_date=datetime.now(),
            end_date=datetime.now() + timedelta(days=1),
            published=True,
            edit_key="1234test"
        )

        self.event_two = Event.objects.create(
            start_date=datetime.now() + timedelta(days=5),
            end_date=datetime.now()+ timedelta(days=6),
            published=True,
            edit_key="skajdhsakjh"
        )

        self.event_three = Event.objects.create(
            start_date=datetime.now() + timedelta(days=3),
            end_date=datetime.now() + timedelta(days=4),
            published=True,
            edit_key="salkdjalskjd"
        )

        self.old_event = Event.objects.create(
            start_date=datetime.now() - timedelta(days=1),
            end_date=datetime.now() - timedelta(days=1),
            published=True
        )

        self.unpublished_event = Event.objects.create(
            start_date=datetime.now(),
            end_date=datetime.now() + timedelta(days=1),
            published=False
        )

        self.data = {
            "venue": "test venue",
            "acts": "test band, test bands",
            "cost": 1.0,
            "description": "this is a test event",
            "email": "test@test.com",
            "start_date": datetime.now(),
            "end_date": datetime.now() + timedelta(days=1),
            "occur":  Event.OCCUR_CHOICES[0][0],
            "download_image": ""
        }

        self.category = EventCategory.objects.create()

    def test_events(self):
        response = self.client.get(reverse('events'))

        self.assertIn(self.event, response.context['events'])
        self.assertNotIn(self.old_event, response.context['events'])
        self.assertNotIn(self.unpublished_event, response.context['events'])
        self.assertIn(self.category, response.context['categories'])

        self.assertEqual(self.event, response.context['events'][0])
        self.assertEqual(self.event_two, response.context['events'][2])
        self.assertEqual(self.event_three, response.context['events'][1])

    def test_detail(self):
        response = self.client.get(
            reverse('event_detail', kwargs={'pk': self.event.id})
        )

        self.assertEqual(self.event, response.context['event'])

    def test_update_event(self):
        # base_url = reverse('update_event', kwargs={"pk": self.event.id}) 
        # self.client.post(base_url + "?key=" + self.event.edit_key, self.data)

        # self.assertEqual("this is a test", self.event.description)
        pass # Is this view used?

    def test_submit_event(self):
        self.data["description"] = "This is a new event"
        response = self.client.post(reverse("submit_event"), self.data)

        self.assertTrue(
            Event.objects.filter(description="This is a new event").exists()
        )

    def test_get_fb_event(self):
        pass
