from datetime import datetime

from django.test import TestCase

from apps.events.forms import EventForm
from apps.events.models import Event


class EventFormsTest(TestCase):
    def setUp(self):
        data = {
            'venue': 'Test Venue',
            'acts': 'CSFTT band',
            'cost': '10.0',
            'start_date': datetime.now(),
            'end_date': datetime.now(),
            'description': 'This is a test event',
            'email': '123@test.com',
            'occur': Event.OCCUR_CHOICES[0][0],
            'published': True
        }

        self.form = EventForm(data)

    def test_submit_form(self):
        self.assertTrue(self.form.is_valid())
