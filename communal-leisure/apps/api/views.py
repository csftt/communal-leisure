from itertools import chain
from datetime import datetime, timedelta
from rest_framework import viewsets, exceptions, mixins

from apps.api.serializers import (EventSerializer,
                                  EventCategorySerializer,
                                  ArticleSerializer,
                                  MagazineSerializer)
from apps.events.models import Event, EventCategory
from apps.articles.models import Article, Magazine
from django.db.models import Q

class EventViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_class = EventSerializer

    def get_queryset(self):
        """
        Optionally restrict the returned events to filter by date
        or category. Dates take the format YYYY-MM-DD.
        """
        after_date = self.request.query_params.get('after_date', None)
        query_from_date = datetime.now()

        if after_date is not None:
            try:
                after_date_as_datetime = datetime.strptime(
                    after_date, "%Y-%m-%d"
                )
            except ValueError:
                raise exceptions.ParseError(
                    detail="Invalid date format. Dates take the format YYYY-MM-DD"
                )
            query_from_date = after_date_as_datetime

        queryset = Event.objects.filter(Q(end_date__gte=query_from_date)|Q(occur__gte=1))
        

        for event in queryset:
            if event.occur == 1:
                while event.start_date < query_from_date:
                    event.start_date = event.start_date + timedelta(days=7)
                    event.end_date = event.end_date + timedelta(days=7)
                event.save()

            if event.occur == 2:
                while event.start_date < query_from_date:
                    event.start_date = event.start_date + timedelta(days=14)
                    event.end_date = event.end_date + timedelta(days=14)
                event.save()

        return queryset 

        #return sorted(chain(single_events,weekly_events,biweekly_events), key=lambda x: x.start_date)


class EventCategoryViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = EventCategory.objects.all()
    serializer_class = EventCategorySerializer


class ArticleViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Article.objects.all()
    serializer_class = ArticleSerializer


class MagazineViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Magazine.objects.filter(published=True)
    serializer_class = MagazineSerializer

