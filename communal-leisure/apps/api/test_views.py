from datetime import datetime, timedelta
import json

from django.test import TestCase
from rest_framework.reverse import reverse
from model_mommy import mommy

from apps.events.models import Event, EventCategory
from apps.articles.models import Article, Magazine


class APIViewsTest(TestCase):
    def setUp(self):
        self.category = mommy.make(
            EventCategory,
            name="Test"
        )

        self.other_category = mommy.make(
            EventCategory,
            name="OtherTest"
        )

        self.event = mommy.make(
            Event,
            published=True,
            start_date=datetime.now() + timedelta(days=1),
            end_date=datetime.now() + timedelta(days=2),
            category=self.category
        )

        self.unpublished_event = mommy.make(
            Event,
            published=False,
            start_date=datetime.now() + timedelta(days=1),
            end_date=datetime.now() + timedelta(days=2),
            category=self.category
        )

        self.event_in_past = mommy.make(
            Event,
            published=True,
            start_date=datetime.now() - timedelta(days=2),
            end_date=datetime.now() - timedelta(days=1),
            category=self.other_category
        )

        self.magazine = mommy.make(
            Magazine,
            published=True
        )

        self.unpublished_magazine = mommy.make(
            Magazine,
            published=False
        )

        self.article = mommy.make(
            Article
        )

    def test_events(self):
        response = self.client.get(
            reverse("events-list"),
            content_type='application/json'
        )

        self.assertEqual(response.status_code, 200)

    def test_unpublished_events(self):
        response = self.client.get(
            reverse("events-list"),
            content_type="application/json"
        )

        self.assertTrue(
            len(json.loads(response.content)) == 2
        )

    def test_events_filtered_by_category(self):
        response = self.client.get(
            reverse("events-list"),
            {"category": "test", },
            content_type="application/json"
        )

        self.assertTrue(
            json.loads(response.content)[0]['pk'] == self.event.pk
        )

        self.assertTrue(
            len(json.loads(response.content)) == 1
        )

    def test_events_filtered_by_date_after(self):
        response = self.client.get(
            reverse("events-list"),
            {"after_date": datetime.now().strftime("%Y-%m-%d"), },
            content_type="application/json"
        )

        self.assertTrue(
            json.loads(response.content)[0]['pk'] == self.event.pk
        )

    def test_events_filtered_by_date_before(self):
        response = self.client.get(
            reverse("events-list"),
            {"before_date": datetime.now().strftime("%Y-%m-%d"), },
            content_type="application/json"
        )

        self.assertTrue(
            json.loads(response.content)[0]['pk'] == self.event_in_past.pk
        )

    def test_events_filtered_by_date_with_invalid_arguments(self):
        response = self.client.get(
            reverse("events-list"),
            {"after_date": datetime.now().strftime("%Y/%m/%d"), },
            content_type="application/json"
        )

        self.assertEqual(response.status_code, 400)

    def test_categories(self):
        response = self.client.get(
            reverse("categories-list"),
            content_type='application/json'
        )

        self.assertEqual(response.status_code, 200)

    def test_articles(self):
        response = self.client.get(
            reverse("articles-list"),
            content_type="application/json"
        )

        self.assertTrue(
            json.loads(response.content)[0]['pk'] == self.article.pk
        )

    def test_magazines(self):
        response = self.client.get(
            reverse("magazines-list"),
            content_type="application/json"
        )

        self.assertTrue(
            json.loads(response.content)[0]['pk'] == self.magazine.pk
        )

    def test_unpublished_magazine(self):
        response = self.client.get(
            reverse("magazines-list"),
            content_type="application/json"
        )

        self.assertTrue(
            len(json.loads(response.content)) == 1
        )
