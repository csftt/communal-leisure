from rest_framework import serializers

from apps.events.models import Event, EventCategory
from apps.articles.models import Article, Magazine


class EventCategorySerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = EventCategory
        fields = ('display_name', 'name')


class EventSerializer(serializers.HyperlinkedModelSerializer):
    category = EventCategorySerializer()

    def create(self, data):
        return Event(**data)

    class Meta:
        model = Event
        fields = (
            'pk',
            'image',
            'description',
            'acts',
            'venue',
            'start_date',
            'end_date',
            'cost',
            'occur',
            'category'
        )


class ArticleSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Article

        # TODO - Add created_by back in
        fields = (
            'pk',
            'article_body',
            'date_created',
            'image',
            'name',
            'attachment',
            'attachment_name'
        )


class MagazineSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Magazine
        fields = ('pk', 'date_created', 'image', 'pdf')
