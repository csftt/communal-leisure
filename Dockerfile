FROM python:3
ENV PYTHONUNBUFFERED 1
ENV DJANGO_SETTINGS_MODULE=config.settings.development
RUN mkdir /communal-leisure
WORKDIR /communal-leisure
ADD requirements.txt /communal-leisure/
RUN pip3 install -r requirements.txt
ADD . /communal-leisure/